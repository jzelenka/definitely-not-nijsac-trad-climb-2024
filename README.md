# 2024 Outdoor autumn CZ trip regions

## General Notes
UIAA is the most abundant grading in the whole Czechia except sandstone, keep it in mind when assessing grades of the routes. There is special sandstone grading (Saxon scale). Regarding destinations - there are always more to-go possibilities. These are some I know or heard a bit about. If you find something interesting yourself, please let me know. I will try to gather more info and add it to the list. For shopping I would just buy something in supermarkets during transfers to keep our supply fresh.

Making open fire in the woods is illegal in Czechia (fine is ~400€(?)). There were already some Duch people charged because of making campfire. The difference between getting fined and being okay is that if you make campfire in the National Park, people will go after you. If you make campfire in the middle of the summer when there was no rain for 14 days and everything is dried up, people will go after you. If you make a fire two days after a rain in some less-protected area or area without protection on a reasonable spot, people do not care at all. Nice example of this is at Dehetnicka Skála. There is a fireplace which seems to be routinely used since I remember the spot (20+ years) and there is a matchbox in the overhang wall next to it, never heard of any complaints, newer saw that signs that someone would try to dismantle the fireplace.

## Schedule proposal
Please take it with a pinch (or rather a bag) of salt! Its just a proposal, we are not bound by it and also if the weather changes we will change our plans accordingly. Option will be chosen based on weather.

### Sooner option
* Monday 14th of October: Going in the afternoon from NL, food on the way (Restaurant? Fast food? Own supplies?). Sleeping close Kryštofovy Hamry (Krušné hory region, ~1hr transport from car to basically middle of nowhere). Sleeping is close to the rock.
* Tuesday 15th of October: Climbing at Kryštof/Hraniční věž (10-25m, ~35routes, UIAA 2-7, granite with grippy texture). Using same site for sleeping as yesterday.
* Wednesday 16th of October: Morning transfer to Perštejn, climbing at Perštejn (up to 40m, ~100 routes, UIAA 1-9). Sleeping at Dehetnická skála
* Thursday 17th of October: Climbing @Dehetnická skála (20+m, ~60 routes, UIAA 1-9 + bouldering), if we get bored there are few other small rocks to explore. Sleeping again at Dehetnická skála if we are satisfied with the room service.
* Friday 18th of October: Morning transfer, climbing @Jickovice (5-35m, ~100 routes, UIAA 1-9, mostly granite crag). Evening transfer to a sleepover close to the next climbing destination.
* Saturday 19th of October: Climbing TBD, sleepover at the place. Or moving to NL if we feel that we had enough.
* Sunday 20th of October: Morning climb if we're up for it, then transfer to NL.

Link to map: https://en.mapy.cz/s/fahudotene (end is purely speculative)

Note: It would be nice to refine the plan, I feel that it should be possible to make a better plan, at the same time if weather forecast is crappy one day, this plan have an inbuilt reserve for it.

### Later option
 * Tuesday 22nd of October: Going in the afternoon from NL, food on the way (Restaurant? Fast food? Own supplies?). Sleeping at Dehetnická Skála (Central bohemia region, near Líšná village).
 * Wednesday 23rd of October: Climbing @Dehetnická skála (20+m, ~60 routes, UIAA 1-9 + bouldering), if we get bored there are few other small rocks to explore. Sleeping again at Dehetnická skála if we are satisfied with the room service.
 * Thursday 24th of October: Morning transfer, climbing @Jickovice (5-35m, ~100 routes, UIAA 1-9, mostly granite crag). Evening transfer to Kryštofovy Hamry
 * Friday 25th of October: Climbing at Kryštof/Hraniční věž (10-25m, ~35routes, UIAA 2-7, granite with grippy texture).
 * Saturday 26th of October: Morning transfer to Perštejn, climbing at Perštejn (up to 40m, ~100 routes, UIAA 1-9). Sleepover location is TBD.
 * Sunday 27th of October: I will go in morning to pick-up my daughter, rest can probably still climb a bit in the morning. Later in the morning trip back to NL.

Link to map (end is again purely speculative): https://en.mapy.cz/s/geladufazo

## And if its going to Rain?
We will have to discuss this.

 * Hiking - Krušky, Jezírka
 * Mines - Amerika, Příbram, Krušky
 * Caves - Koněprusky? Něco dalšího?

## Regions

## Southern Bohemia

Nice video, unfortunately without subtitles: https://www.youtube.com/watch?v=4jwBr2rbqK8 (you can skip the talking, check the rocks). I am in contact with the Guide who is describing the crags in this region in that video (Vít Novák, UIAGM, he also maintains the crags there). I can ask him for recommendations regarding the region. He can also be booked/hired to get some training if he is not in Alps or busy in other ways (~80-100€/capita/day).

### Sleeping

 * **Hotel Mamut - rock** - overhang cliff, fireplace underneath, should work, but looks a bit ugly because of the soot from the fireplace on the overhang.

 ![hotel mamut - picture taken by Petr Jandík](https://www.horyinfo.cz/image/gallery_clanky_cesty/20130619174601_umamuta/20130619174601_umamuta13.jpg)

 *picture taken from https://www.horyinfo.cz/view.php?cisloclanku=2013060013&nazevclanku=skaly-u-mamuta*

 * **Chostník - rock** - fireplace few meters from rock in the woods, nice vibe, rain protection needed (overhang is good in case of rain during the day, too small for a group to sleep in)

 ![Choustník rock - picture from unknown author](https://www.skalnioblasti.cz/foto/foto-20201224145232816400.jpg)

 *picture taken from https://www.skalnioblasti.cz/5_index.asp?cmd=6&sektor_id=370 , fireplace is visible in front of the tree on the bottom left corner*

 * **Choustník - castle ruins** - for 80€, the whole castle ruins can be rented (two youtube videos in this link: https://www.obec-choustnik.cz/zivot-v-obci/hrad-choustnik/)

 ![Coustnik ruins - pciture from unknown author](https://turistickamapa.cz/data_fotos/zricenina-hradu-choustnik-2021_13_7-145345.jpg)


### Possible crags

 * **U mamuta** - https://www.horosvaz.cz/skaly-sektor-1121/ - Hotel Mamut sleepover/roof climbing is the second rock "from left" on the drawing. Nice crag next to a river, quiet, good vibe. Routes might feel short for more advanced climbers. Bolts-free in sections where reliable trad protection can be placed (thus III and IV usually without bolts). Pine trees servers as the Anchor points. Some pics in an old article - https://www.horyinfo.cz/view.php?cisloclanku=2013060013&nazevclanku=skaly-u-mamuta .
 * **Choustník** - https://www.horosvaz.cz/skaly-sektor-31/ - Nice calm crag, once again routes might feel short to some. Topo: https://www.horosvaz.cz/res/archive/402/067343.pdf?seek=1631007356 . Bolted in same philosophy as U mamuta. Anchors either from trees on top, or single bolts/rings.
 * **Pod Duhou** - never been there, rock looks appealing - https://www.horosvaz.cz/skaly-skala-18072/
 * **Jickovice** - its between southern and central bohemia. https://www.horosvaz.cz/skaly-sektor-461/ , got recommended as to-go destination nearby when asking where to get some non-sandstone crag climbing experience (its Granite).



## Central Bohemia

### Sleeping

 * **Dehetnická skála** - https://www.horosvaz.cz/skaly-sektor-266/ - you saw the picture of the benches/fireplace on Signal.
 * There are other sleeping possibilities, usually a fireplace somewhere in the middle of the woods out of sight from the roads.

### Possible crags

 * **Dehetnická skála** - https://www.horosvaz.cz/skaly-sektor-266/ - My "home crag" It feels small and cozy to me. There are some bouldering possibilities. Link to boulder topos + pics of bouldering in this link - https://www.lezec.cz/clanek.php?key=17839&nazev=dehetnicka_skala_a_bouldering . Climbing is trad, routes are ~20m (guesstimate), bolted in similar fashion as "U mamuta." Anchor point made from single bolt on top.
 * **Alkazar** - https://www.horosvaz.cz/skaly-sektor-161/ - heard that the holds are polished. Should be sport-climbing oriented destination. Never been there. Its a former mine. Fun fact: its documented that in the brick-closed domes there is radioactive-waste from research institutes, which was buried there ~30 years ago.
 * **Na hřebenech** - https://www.horosvaz.cz/skaly-sektor-137/ - one sector which I would like to explore out of curiosity. I hiked few times around, its some random rocks/pillars in the woods which seems to be there "out of nowhere." Topos should be in the boxes at the top of the rocks. Its right next to Dehetnická skála (3km).



## Krušné Hory
My favorite hiking destination. Because of the history, the region was strongly depopulated in 40', which means that there is more space for nature. Most of the time you are just somewhere "in the middle of the woods."

### Sleeping
I would propose to just sleep in the woods a bit further from the villages. On multiple occasions We've slept in the "middle of nowhere" and it was always just fine. Only complication might be the water management, there is both cattle and ore mining (silver, copper, tin), so the surface water quality might vary from place to place.

### Possible crags

Disclaimer: I got a topo of the whole region. We can just discuss in the evening during the trip and go.

* **Perštejn** - approximately ~100+ routes(?), 20-30m. Routes are bolted on hard places. Mostly face climbing, I've seen a crack here and there. The stone is metamorphed granite so the structure and texture is quite appealing. I have checked the rock just visually and tested few holds close to the ground. Some of the holds felt a bit too polished for my taste but still its very rough compared to some of the crags in Belgium. Links: https://www.horosvaz.cz/skaly-sektor-479/ , https://www.horyinfo.cz/view.php?cisloclanku=2006110014 . I got some photos, will post them to the Signal group. I would not sleep too close to it as it felt too crowded for my taste.
* **Mnich** - example of a small rock in the region. https://www.horosvaz.cz/skaly-skala-16013/ Small trad-oriented crag with a few routes in middle of a pristine nature Area is protected as a "Natural Monument." Climbing, walking everywhere and bivy is allowed, but in all cases you should be cautious and mindful of nature (ie limit activities causing rock erosion, do not collect/harm Arctostaphylos uva-ursi which grows there, preserve the nature as much as possible). No Magnesium, be nice to the rock. The approach from bottom is a bit demanding, but it was in a joyful nature (free walk in a rocky/wood terrain, ~400m, elevation of 177m, I guess ~30-40minutes with gear). Approach from top should be much easier, but it might miss a bit of the genius loci.
* **Kryštof (Kryštofovy Hamry)** - https://www.horosvaz.cz/skaly-sektor-225/ . Beautiful rock texture - the stone is super gripy and not polished at all. Bolting is in the typical czech semi-trad style. Its ~35 routes with grades ranging from 3 to 7. Few cracks, mostly vertical face climbing with few routes with roof. Rock looks really cozy to me. Approach is ~45 minutes of walk. There are spots suitable for a nice bivak sleepover close-by.
* **Svatošské skály** - Iconic well known crag. https://www.horosvaz.cz/skaly-sektor-261/ There is a myth calling the rock formation "Petrified wedding." Should be mostly trad, got topo (picture is below).
* **Bořeň** - One of a very few Multipitches in Czechia. https://www.horosvaz.cz/skaly-oblast-82/ One of the most important non-sandstone crags in Czechia. Video: Once again, no EN subtitles (sry), Link starts where they start to discuss the rock/route https://youtu.be/j24kgNzbzXc?feature=shared&t=321 . I do not have topo of this one, there are some topos online.

![Svatošky from ČHS](https://www.horosvaz.cz/res/archive/309/052234.jpg?seek=1578739480)

*picture from Svatošské skály - Bride + Husband from the petrified wedding - an old topo*


## Sandstone

It is very very specific climbing. Except of a few places the community want to keep the trad climbing style "clean." This means "no overbolting" (2 or 3 bolts per route is standard), no magnesium, and because the rock is often soft no metal protection (cams/nuts/tricams) is allowed. There are easy routes too, so climbing can be very simple, but because of missing protection the climbing is often a bit more serious.

I asked a friend. There will probably be a possibility to get some people who knows the stuff to go with us. It wont be "for sure" till the moment we are on the way to sandstone. Also because the stone is fragile it cannot be climbed unless it is dry. And as it is porous, it takes sometimes a while to dry it up.

 * **Prachovské skály** - https://www.horosvaz.cz/skaly-oblast-1/ - First documented ascent is from 1907, but there were probably few ascents done by Sachsen climbers already few years before. This is one of the oldest sandstone climbing crags in Czechia. Lots and lots of sandstone climbing. Sandstone everywhere, fixed points far from each other. Parking is 4€/car/day, entrance is 4€/person/day (its a national park). I was there today (7/26) and because of the summer holiday it was crowded, but still, the rocks were simply breathtaking. I expect that in October the place will be much less crowded. Climbing is allowed there each year from April till 31st of October. For visuals check the video: https://www.youtube.com/watch?v=GO8lg9HSRkw (sorry for the missing subs).

 ![Prachov rocks](https://hradec.rozhlas.cz/sites/default/files/styles/cro_16x9_tablet/public/images/a31831ed54442616ede9f7703a38569c.jpeg)

 * **Adršpach** - Isn't it the region where Lords of Trad with Adam Ondra were recorded?

 * **Elbe valley** - sandstone with some sport routes.

 * Other places (probably something in Český ráj and also there are other spots...)


## Bad weather activities

What to do if its rainy?

  * **Climb elsewhere** - As long as we see that the rain is localized we might just move around the country.
  * **Hikes** - Terrain is not bad for easy relaxed hiking. Do not expect mountaineering, you would be disappointed. :D
  * **Mines** - I had some contacts how to get underground with a guide (+- private tours). If we want to have mines as a bad-weather option I need to know in advance.
  * **Caves** - Its a bit too touristy and unfortunately also quite often closed outside of holidays.
